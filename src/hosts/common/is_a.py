from __future__ import absolute_import, division, print_function


def List(obj):
    return isinstance(obj, list)
